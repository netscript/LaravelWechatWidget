<?php
/**
 * Project Widget
 * file: Facade.php
 * User: weblinuxgame
 * Date: 2019/5/28
 * Time: 18:58
 */

namespace WebLinuxGame\Laravel\WeChat\Widget;

use Illuminate\Support\Facades\Facade as BaseFacade;

/**
 * 门脸
 * Class Facade
 * @package WebLinuxGame\Laravel\WeChat\Widget
 */
class Facade extends BaseFacade
{
    /**
     * 获取门脸key
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return ConstEnum::APP_NAME;
    }

}
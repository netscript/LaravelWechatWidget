<?php

namespace WebLinuxGame\Laravel\WeChat\Widget;

use WebLinuxGame\WeChat\Widget\Application;
use Illuminate\Support\ServiceProvider as BaseServiceProvider;

/**
 * 服务
 * Class ServiceProvider
 * @package App\Providers
 */
class ServiceProvider extends BaseServiceProvider
{

    protected $defer = true; // 延迟加载服务

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        if ($this->isBooted()) {
            $this->publishes([
                __DIR__ . '/config/config.php' => config_path(ConstEnum::CONFIG_FILE),
            ], 'config');
        }
    }

    /**
     * 是否已经安装引导
     * @return bool
     */
    private function isBooted()
    {
        $env = $this->app[ConstEnum::CONFIG_CONTAINER] ?? [];
        if (!empty($env) && !empty($env[ConstEnum::CONFIG_SCOPE])) {
            return false;
        }
        if (!empty($this->app[ConstEnum::CONFIG_ROOT]) && !empty($this->app[ConstEnum::CONFIG_ROOT][ConstEnum::CONFIG_SCOPE])) {
            return false;
        }
        return empty(config(ConstEnum::CONFIG_SCOPE)) ? true : false;
    }

    /**
     * 服务注册
     * Register services.
     */
    public function register()
    {
        // 默认单例
        $this->app->singleton(ConstEnum::APP_NAME, function ($app) {
            $config = Env::getCreateConfig($app);
            return new Application($config['main'] ?? [], $config['defaults'] ?? []);
        });
        // 创建函数
        $this->app->singleton(ConstEnum::APP_CREATE_FUNC, function ($app) {
            return function (string $id) use ($app) {
                $config = Env::getCreateConfig($app, $id);
                return new Application($config['main'] ?? [], $config['defaults'] ?? []);
            };
        });
        // 配置对象
        if (empty($this->app[ConstEnum::CONFIG_CONTAINER])) {
            $this->app->singleton(ConstEnum::CONFIG_CONTAINER, function ($app) {
                return Env::getInstance($app);
            });
        }
    }

    /**
     * @return array
     */
    public function provides()
    {
        return [ConstEnum::APP_NAME, ConstEnum::APP_CREATE_FUNC];
    }

}

<?php
/**
 * Project Widget
 * file: config.php
 * User: weblinuxgame
 * Date: 2019/5/28
 * Time: 18:13
 */

return [
    /*
   * 默认配置，将会合并到各模块中
   */
    'defaults' => [
        /*
         * 指定 API 调用返回结果的类型：array(default)/collection/object/raw/自定义类名
         */
        'response_type' => 'array',

        /*
         * 使用 Laravel 的缓存系统
         */
        'use_laravel_cache' => false,

        /*
         * 日志配置
         *
         * level: 日志级别，可选为：
         *                 debug/info/notice/warning/error/critical/alert/emergency
         * file：日志文件位置(绝对路径!!!)，要求可写权限
         */
        'log' => [
            'default' => 'dev', // 默认使用的 channel，生产环境可以改为下面的 prod
            'channels' => [
                // 测试环境
                'dev' => [
                    'driver' => 'single',
                    'path' => env('WECHAT_LOG_FILE', 'logs/wechat.log'),
                    'level' => 'debug',
                ],
                // 生产环境
                'prod' => [
                    'driver' => 'daily',
                    'path' => env('WECHAT_LOG_FILE', 'logs/wechat.log'),
                    'level' => 'info',
                ],
            ],
        ],
    ],

    /*
     * 小程序
     */
    'mini_program' => [
        'default' => [
            'app_id' => env('WECHAT_MINI_PROGRAM_APP_ID', ''),
            'secret' => env('WECHAT_MINI_PROGRAM_SECRET', ''),
            'token' => env('WECHAT_MINI_PROGRAM_TOKEN', ''),
            'aes_key' => env('WECHAT_MINI_PROGRAM_AES_KEY', ''),
            'widget_aes_key' => env('WECHAT_MINI_PROGRAM_AES_KEY', ''),
            'request_type' => env('WECHAT_MINI_PROGRAM_WIDGET_RET', 'json'),
        ],
    ],

    /*
    * 公众号
    */
    'official_account' => [
        'default' => [
            'app_id' => env('WECHAT_OFFICIAL_APPID', 'your-app-id'),         // AppID
            'secret' => env('WECHAT_OFFICIAL_APPSECRECT', 'your-app-secret'),    // AppSecret
            'token' => env('WECHAT_OFFICIAL_ACCOUNT_TOKEN', 'your-token'),           // Token
            'aes_key' => env('WECHAT_OFFICIAL_AES_KEY', 'WECHAT_AES_KEY'),                 // EncodingAESKey

            /*
             * OAuth 配置
             *
             * scopes：公众平台（snsapi_userinfo / snsapi_base），开放平台：snsapi_login
             * callback：OAuth授权完成后的回调页地址(如果使用中间件，则随便填写。。。)
             */
            'oauth' => [
                'scopes'   => array_map('trim', explode(',', env('WECHAT_OFFICIAL_ACCOUNT_OAUTH_SCOPES', 'snsapi_userinfo'))),
                'callback' => env('WECHAT_OFFICIAL_ACCOUNT_OAUTH_CALLBACK', '/wechat/oauth/callback'),
            ],
        ],
    ],
];
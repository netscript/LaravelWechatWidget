<?php
/**
 * Project Widget
 * file: Env.php
 * User: weblinuxgame
 * Date: 2019/5/28
 * Time: 19:31
 */

namespace WebLinuxGame\Laravel\WeChat\Widget;


use Closure;
use ArrayAccess;
use Illuminate\Contracts\Foundation\Application;

/**
 * 扩展配置获取
 * Class Env
 * @package WebLinuxGame\Laravel\WeChat\Widget
 */
class Env implements ArrayAccess
{

    /**
     * @var Application $app
     */
    protected static $app;
    protected static $instance;

    /**
     * Env constructor.
     * @param null $app
     */
    protected function __construct($app = null)
    {
        self::app($app);
    }

    /**
     * 获取单例
     * @param null $app
     * @return Env
     */
    public static function getInstance($app = null)
    {
        if (empty(self::$instance)) {
            self::$instance = new static($app);
        }
        return self::$instance;
    }

    /**
     * 获取app
     * @param Application|null $app
     * @return \Illuminate\Foundation\Application|mixed
     */
    public static function app(Application $app = null)
    {
        if (empty(self::$app)) {
            if (!empty($app)) {
                self::$app = $app;
            }
            self::$app = \app();
        }
        return self::$app;
    }

    /**
     * 获取配置
     * @param string $key
     * @param null $default
     * @return mixed|null
     */
    public static function config(string $key, $default = null)
    {
        $value = $default;
        $handler = env(ConstEnum::CONFIG_HANDLER);
        if (!empty($handler) && is_callable($handler)) {
            $handler = Closure::fromCallable($handler);
            $value = $handler($key);
        }
        return empty($value) ? $default : $value;
    }

    /**
     * 获取配置
     * @param Application|null $app
     * @return \Illuminate\Config\Repository|mixed|null
     */
    public static function scope(Application $app = null)
    {
        $app = self::app($app);
        $config = config(ConstEnum::CONFIG_SCOPE, []);
        if (empty($config)) {
            $env = $app[ConstEnum::CONFIG_CONTAINER] ?? [];
        }
        if (!empty($env[ConstEnum::CONFIG_SCOPE]) && is_array($env[ConstEnum::CONFIG_SCOPE])) {
            $config = $env[ConstEnum::CONFIG_SCOPE];
        }
        return empty($config) ? self::config(ConstEnum::CONFIG_SCOPE, []) : $config;
    }

    /**
     * 获取创建配置
     * @param $app
     * @param string $name
     * @return array
     */
    public static function getCreateConfig(Application $app, string $name = 'default'): array
    {
        $config = self::scope($app);
        $main = $config[ConstEnum::CONFIG_MAIN_KEY] ?? Env::config(ConstEnum::CONFIG_MAIN_KEY, []);
        $defaults = $config[ConstEnum::CONFIG_APPEND_KEY] ?? Env::config(ConstEnum::CONFIG_APPEND_KEY, []);
        if (!empty($main) && is_array($main) && !empty($main[$name])) {
            $main = $main[$name];
        }
        return compact('main', 'defaults');
    }

    /**
     * 防止克隆
     */
    public function __clone()
    {
    }

    /**
     * Whether a offset exists
     * @link https://php.net/manual/en/arrayaccess.offsetexists.php
     * @param mixed $offset <p>
     * An offset to check for.
     * </p>
     * @return boolean true on success or false on failure.
     * </p>
     * <p>
     * The return value will be casted to boolean if non-boolean was returned.
     * @since 5.0.0
     */
    public function offsetExists($offset)
    {
        if (!is_null($this->offsetGet($offset))) {
            return true;
        }
        return false;
    }

    /**
     * Offset to retrieve
     * @link https://php.net/manual/en/arrayaccess.offsetget.php
     * @param mixed $offset <p>
     * The offset to retrieve.
     * </p>
     * @return mixed Can return all value types.
     * @since 5.0.0
     */
    public function offsetGet($offset)
    {
        $value = config($offset, null);
        if (empty($value)) {
            return self::config($offset);
        }
        return $value;
    }

    /**
     * Offset to set
     * @link https://php.net/manual/en/arrayaccess.offsetset.php
     * @param mixed $offset <p>
     * The offset to assign the value to.
     * </p>
     * @param mixed $value <p>
     * The value to set.
     * </p>
     * @return void
     * @since 5.0.0
     */
    public function offsetSet($offset, $value)
    {
        config([$offset => $value]);
    }

    /**
     * Offset to unset
     * @link https://php.net/manual/en/arrayaccess.offsetunset.php
     * @param mixed $offset <p>
     * The offset to unset.
     * </p>
     * @return void
     * @since 5.0.0
     */
    public function offsetUnset($offset)
    {
        config([$offset => null]);
    }
}
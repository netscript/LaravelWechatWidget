<?php
/**
 * Project Widget
 * file: ConstEnum.php
 * User: weblinuxgame
 * Date: 2019/5/28
 * Time: 19:12
 */

namespace WebLinuxGame\Laravel\WeChat\Widget;

/**
 * Interface ConstEnum
 * @package WebLinuxGame\Laravel\WeChat\Widget
 */
interface ConstEnum
{
    const APP_NAME = 'wechat_widget';// 应用facade 名
    const CONFIG_SCOPE = 'wechat';// 对应应用配置作用域
    const CONFIG_ROOT = 'config';// 扩展配置容器名
    const CONFIG_CONTAINER = 'env.config';// 扩展配置容器名
    const CONFIG_MAIN_KEY = 'mini_program';// 对应配置主键名
    const CONFIG_APPEND_KEY = 'defaults';// 默认相关配置
    const CONFIG_FILE = 'wechat.php';// 配置文件名
    const APP_CREATE_FUNC = 'call_widget';// 构建函数名
    const CONFIG_HANDLER = 'WECHAT_CONFIG_CALL'; // 拓展配置获取方式
}
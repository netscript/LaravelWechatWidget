基于 weblinuxgame/wechat-widget 的laravel 封装包
------------------------------------------------

>  weblinuxgame/wechat-widget@1.0.2  
 
  
> laravel@5.6.*  


> 支持 facade [wechat_widget]


> 包内常量 封装类  WebLinuxGame\Laravel\WeChat\Widget\ConstEnum  

  包括 : Facade-Name , Config-key,Extend-Config-Handler   
  

> 例子:  

    /**
     * 返回创建好的实例
     *@var \WebLinuxGame\WeChat\Widget\Application $oWidget
     **/
    $oWidget = app('wechat_widget');
    
    // $oWidget->widget
    // $oWidget->server
    // $oWidget->widget_messenger
    // $oWidget->widget_encryptor
    
    // 返回创建函数
    $creatorFunc = app('wechat_widget_call');
        
    $oWidget = $creatorFunc($config);
    
  
   

  
